#include <stdio.h>
#include <math.h>

int main() {
	double a;
	
	printf("Введите a: "); scanf("%lf", &a);
	
	printf("S = %lf\n", pow(a, 2));
	
	return 0;
}
