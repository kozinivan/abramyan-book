#include <stdio.h>

int main() {
	double a, b;
	
	printf("Введите a: "); scanf("%lf", &a);
	printf("Введите b: "); scanf("%lf", &b);
	
	printf("S = %lf\nP = %lf\n", a * b, 2 * (a + b));
	
	return 0;
}
