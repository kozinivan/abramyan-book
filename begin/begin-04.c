#include <stdio.h>

#define PI 3.14

int main() {
	double d;
	
	printf("Введите d: "); scanf("%lf", &d);
	
	printf("L = %lf\n", PI * d);
	
	return 0;
}
