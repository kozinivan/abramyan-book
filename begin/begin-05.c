#include <stdio.h>
#include <math.h>

int main() {
	double a;
	
	printf("Введите a: "); scanf("%lf", &a);
	
	printf("V = %lf\nS = %lf\n", pow(a, 3), 6 * pow(a, 2));
	
	return 0;
}
