#include <stdio.h>

int main() {
	double a, b, c;
	
	printf("Введите a: "); scanf("%lf", &a);
	printf("Введите b: "); scanf("%lf", &b);
	printf("Введите c: "); scanf("%lf", &c);
	
	printf("V = %lf\nS = %lf\n", a * b * c, 2 * (a * b + b * c + a * c));
	
	return 0;
}
