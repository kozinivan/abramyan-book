#include <stdio.h>
#include <math.h>

#define PI 3.14

int main() {
	double R;
	
	printf("Введите R: "); scanf("%lf", &R);
	
	printf("L = %lf\nS = %lf\n", 2 * PI * R, PI * pow(R, 2));
	
	return 0;
}
