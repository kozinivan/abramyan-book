#include <stdio.h>

int main() {
	double a, b;
	
	printf("Введите a: "); scanf("%lf", &a);
	printf("Введите b: "); scanf("%lf", &b);
	
	printf("Среднее арифметическое: %lf\n", (a + b) / 2);
	
	return 0;
}
