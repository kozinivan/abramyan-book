#include <stdio.h>
#include <math.h>

int main() {
	double a, b;
	
	printf("Введите первое число: "); scanf("%lf", &a);
	printf("Введите второе число: "); scanf("%lf", &b);
	
	if (a == 0 || b == 0) {
		printf("Ошибка, числа не могут быть нулевыми!\n");
	} else {
		printf("Сумма чисел: %.2lf + %.2lf = %.2lf\n", a, b, a + b);
		printf("Разность чисел: %.2lf - %.2lf = %.2lf\n", a, b, a - b);
		printf("Произведение чисел: %.2lf * %.2lf = %.2lf\n", a, b, a * b);
		printf("Частное квадратов чисел: %.2lf^2 / %.2lf^2 = %.2lf\n", a, b, pow(a, 2) / pow(b, 2));
	}
	
	return 0;
}
