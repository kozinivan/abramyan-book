#include <stdio.h>
#include <math.h>

int main() {
	double a, b, c;
	
	printf("Введите a: "); scanf("%lf", &a);
	printf("Введите b: "); scanf("%lf", &b);
	
	c = sqrt(pow(a, 2) + pow(b, 2));
	
	printf("c = %lf\nP = %lf\n", c, a + b + c);
	
	return 0;
}
