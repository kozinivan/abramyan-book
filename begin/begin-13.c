#include <stdio.h>
#include <math.h>

#define PI 3.14

int main() {
	double R1, R2, S1, S2, S3;
	
	printf("Введите R1: "); scanf("%lf", &R1);
	printf("Введите R2: "); scanf("%lf", &R2);
	
	if (R1 <= R2) {
		printf("Ошибка, R1 должно быть больше R2!");
	} else {
		S1 = PI * pow(R1, 2);
		S2 = PI * pow(R2, 2);
		S3 = S1 - S2;
		
		printf("S1 = %lf\nS2 = %lf\nS3 = %lf\n", S1, S2, S3);
	}
	
	return 0;
}
