#include <stdio.h>
#include <math.h>

#define PI 3.14

int main() {
	double L, S, R;
	
	printf("Введите L: "); scanf("%lf", &L);
	
	R = L / (2 * PI);
	S = PI * pow(R, 2);
	
	printf("R = %lf\nS = %lf\n", R, S);
	
	return 0;
}