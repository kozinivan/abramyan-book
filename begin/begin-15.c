#include <stdio.h>
#include <math.h>

#define PI 3.14

int main() {
	double S, D, L;
	
	printf("Введите S: "); scanf("%lf", &S);
	
	D = 2 * sqrt(PI * S);
	L = PI * D;
	
	printf("D = %lf\nL = %lf\n", D, L);
	
	return 0;
}