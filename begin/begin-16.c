#include <stdio.h>
#include <math.h>

int main() {
	double x1, x2;
	
	printf("Введите x1: "); scanf("%lf", &x1);
	printf("Введите x2: "); scanf("%lf", &x2);
	
	printf("Результат: %lf\n", abs(x2 - x1));
	
	return 0;
}