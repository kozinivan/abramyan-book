#include <stdio.h>

int main() {
	double A, B, C;
	
	printf("Введите A: "); scanf("%lf", &A);
	printf("Введите B: "); scanf("%lf", &B);
	printf("Введите C: "); scanf("%lf", &C);
	
	printf("AB = %lf\nBC = %lf\nAB + BC = %lf\n", A * B, B * C, A * B + B * C);
	
	return 0;
}