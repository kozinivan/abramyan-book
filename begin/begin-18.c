#include <stdio.h>

int main() {
	double A, B, C;
	
	printf("Введите A: "); scanf("%lf", &A);
	printf("Введите B: "); scanf("%lf", &B);
	printf("Введите C: "); scanf("%lf", &C);
	
	if (A < C || C > B) {
		printf("Ошибка, C должно быть между A и B!\n");
	} else {
		printf("AB + BC = %lf\n", A * B + B * C);
	}
	
	return 0;
}