#include <stdio.h>
#include <math.h>

int main() {
	double x1, y1, x2, y2;
	double x3, y3, x4, y4;
	double AB, BC, CD, DA;
	double P, S;
	
	/*
		A (x1, y1)
		B (x4, y4)
		C (x2, y2)
		D (x3, y3)
	*/
	
	printf("Введите x1: "); scanf("%lf", &x1);
	printf("Введите y1: "); scanf("%lf", &y1);
	printf("Введите x2: "); scanf("%lf", &x2);
	printf("Введите y2: "); scanf("%lf", &y2);
	
	x3 = x1;
	y3 = y2;
	
	x4 = x2;
	y4 = y1;
	
	AB = fabs(x4 - x1);
	BC = fabs(y4 - y2);
	CD = fabs(x3 - x2);
	DA = fabs(y1 - y3);
	
	P = 2 * (AB + BC);
	S = AB * BC;
	
	printf("==== Прямоугольник ====\nДаны координаты:\nA (%.1lf, %.1lf)\nC (%.1lf, %.1lf)\n\nНайденные координаты:\nB (%.1lf, %.1lf)\nD (%.1lf, %.1lf)\n\nДлины сторон прямоугольника:\nAB = %.1lf\nBC = %.1lf\nCD = %.1lf\nDA = %.1lf\n\nP = 2 * (%.1lf + %.1lf) = %.1lf\nS = %.1lf * %.1lf = %.1lf\n", x1, y1, x2, y2, x4, y4, x3, y3, AB, BC, CD, DA, AB, BC, P, AB, BC, S);
	
	return 0;
}