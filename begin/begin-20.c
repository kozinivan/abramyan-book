#include <stdio.h>
#include <math.h>

int main() {
	double x1, y1, x2, y2;
	
	printf("Введите x1: "); scanf("%lf", &x1);
	printf("Введите y1: "); scanf("%lf", &y1);
	printf("Введите x2: "); scanf("%lf", &x2);
	printf("Введите y2: "); scanf("%lf", &y2);
	
	printf("sqrt( (%.1lf - %.1lf)^2 + (%.1lf - %.1lf)^2 ) = %.2lf\n", x2, x1, y2, y1, sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2)));
	
	return 0;
}