#include <stdio.h>

int main() {
	
	int n, k, x, y;
	float r = 0, nk;
	
	printf("N: "); scanf("%i", &n);
	printf("K: "); scanf("%i", &k);
	
	for (x = 1; x <= n; x++) {
		nk = x;
		for (y = 1; y <= k; y++) {
			nk *= x;
		}
		r += nk;
	}
	
	printf("%f\n", r);
	
	return 0;
	
}
