#include <stdio.h>

int main() {
	
	int x, y;
	
	printf("x: "); scanf("%i", &x);
	printf("y: "); scanf("%i", &y);
	
	if ( (x == 0) && (y == 0) )
	{
		printf("0");
	} else if (x == 0) {
		printf("1");
	} else if (y == 0) {
		printf("2");
	} else {
		printf("3");
	}
	
	return 0;
	
}
