#include <stdio.h>

int main() {
	
	int M, N, i, j;
	
	printf("N: "); scanf("%i", &N);
	printf("M: "); scanf("%i", &M);
	
	int a[M][N];
	
	for (i = 0; i <= M - 1; i++) {
		printf("(%i)\n", i);
		for (j = 0; j <= N - 1; j++) {
			printf("%i: ", j); scanf("%i", &a[i][j]);
		}
	}
	
	for (i = 0; i < M; i++) {
		for (j = 1; j < N; j+=2) {
			printf("%i, ", a[i][j]);
		}
		printf("\n");
	}
/*
	i = 0;
	while (i < M) {
		i += 2;
		for (j = 0; j <= N - 1; j++) {
			printf(" : %i", *a[i, j]);
		}
		printf("\n");
	}
*/	
	return 0;
	
}
