#include <stdio.h>

int main() {
	
	int i;
	float r, R;
	
	for (i = 1; i <= 10; i++) {
		printf("%i: ", i); scanf("%f", &r);
		R += r;
	}
	
	printf("%f\n", R / 10);
	
	return 0;
	
}
